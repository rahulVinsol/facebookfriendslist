//
//  FriendInfoView.swift
//  FacebookPhotos
//
//  Created by Rahul Rawat on 23/06/21.
//

import SwiftUI

struct FriendInfoView: View {
    @Environment(\.presentationMode) var presentationMode
    var friend: Friend
    
    var body: some View {
        GeometryReader { reader in
            VStack(alignment: .center, spacing: 0) {
                Button(action: {
                    presentationMode.wrappedValue.dismiss()
                }, label: {
                    ClearBackgroundView()
                        .frame(width: reader.size.width, height: reader.size.height * 0.4)
                })
                
                VStack {
                    AsyncImage(url: friend.picture.data.url, cacheKey: friend.id, placeholder: Color.black) { image in
                        Image(uiImage: image)
                            .resizable()
                    }
                    .clipShape(Circle()).overlay(Circle().stroke(Color.gray, lineWidth: 4))
                    .shadow(radius: 7)
                    .aspectRatio(contentMode: .fit)
                    .frame(width: reader.size.width * 0.7, alignment: .center)
                    .padding(.top, -reader.size.height * 0.2)
                    
                    Text("\(friend.name)")
                        .fontWeight(.black)
                        .font(.largeTitle)
                        .padding(.bottom, 10)
                        .padding(.top, 50)
                        .frame(alignment: .center)
                    
                    Text("\(friend.id)")
                        .fontWeight(.heavy)
                        .font(.title)
                    
                    Spacer()
                }
                .background(Color.white.cornerRadius(20, corners: [.topLeft, .topRight]).frame(width: reader.size.width))
                .edgesIgnoringSafeArea(.bottom)
            }.frame(width: reader.size.width, height: reader.size.height, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
        }
    }
}

struct ClearBackgroundView: UIViewRepresentable {
    func makeUIView(context: Context) -> some UIView {
        let view = UIView()
        DispatchQueue.main.async {
            view.superview?.superview?.backgroundColor = .clear
        }
        return view
    }
    func updateUIView(_ uiView: UIViewType, context: Context) {
        
    }
}

struct RoundedCorner: Shape {

    var radius: CGFloat = .infinity
    var corners: UIRectCorner = .allCorners

    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}

extension View {
    func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
        clipShape(RoundedCorner(radius: radius, corners: corners))
    }
}
