//
//  LoginViewModel.swift
//  FacebookPhotos
//
//  Created by Rahul Rawat on 22/06/21.
//

import Foundation
import FBSDKLoginKit
import Combine

class LoginViewModel: ObservableObject {
    private var manager = LoginManager()
    private var cancellables = Set<AnyCancellable>()
    var defaultsHelper = DefaultsHelper.shared
    
    @Published var isLoggedIn: Bool = false
    @Published var email: String = ""
    @Published var userId: String = ""
    
    @Published var friends: [Friend] = []
    
    init() {
        defaultsHelper.$isLoggedIn
            .sink {[weak self] isLoggedIn in
                self?.isLoggedIn = isLoggedIn
            }.store(in: &cancellables)
        
        defaultsHelper.$email
            .sink {[weak self] email in
                self?.email = email
            }.store(in: &cancellables)
        
        defaultsHelper.$userId
            .sink {[weak self] userId in
                self?.userId = userId
            }.store(in: &cancellables)
    }
    
    deinit {
        cancellables.removeAll()
    }
    
    func login() {
        manager.logIn(permissions: ["public_profile", "email", "user_friends"], from: nil) { (result, err) in
            if err != nil {
                print(err!.localizedDescription)
                return
            }
            
            if result?.isCancelled == false {
                self.defaultsHelper.isLoggedIn = true
                
                let request = GraphRequest(graphPath: "me", parameters: ["fields" : "email"])
                
                request.start { [weak self] (_, res, _) in
                    guard let self = self else { return }
                    guard let profileData = res as? [String : Any] else { return }
                    
                    self.defaultsHelper.email = profileData["email"] as! String
                    self.defaultsHelper.userId = profileData["id"] as! String
                    
                    self.fetchFriends()
                }
            }
        }
    }
    
    func fetchFriends() {
        if !defaultsHelper.isLoggedIn {
            return
        }
        let request = GraphRequest(graphPath: "me/friends", parameters: ["fields":"picture,name,birthday,email"])
        
        request.start { [weak self] (_, res, error) in
            guard let self = self else { return }
            guard let profileData = res as? [String : Any] else { return }
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: profileData, options: .prettyPrinted)
                
                let friendsInfo = try JSONDecoder().decode(FriendsInfo.self, from: Data(jsonData))
                
                self.friends = friendsInfo.data
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    func logout() {
        manager.logOut()
        defaultsHelper.clearAll()
        friends.removeAll()
    }
}
