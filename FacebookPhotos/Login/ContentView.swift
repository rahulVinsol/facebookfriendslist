//
//  ContentView.swift
//  FacebookPhotos
//
//  Created by Rahul Rawat on 17/06/21.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject private var viewModel = LoginViewModel()
    
    var body: some View {
        NavigationView {
            VStack(spacing: 25) {
                
                Spacer()
                
                Button(action: {
                    if viewModel.isLoggedIn {
                        viewModel.logout()
                    } else {
                        viewModel.login()
                    }
                }, label: {
                    Text(viewModel.isLoggedIn ? "Log Out" :"Login" )
                        .fontWeight(.bold)
                        .foregroundColor(.white)
                        .padding(.vertical, 15)
                        .padding(.horizontal, 35)
                        .background(Color.blue)
                        .clipShape(Capsule())
                })
                
                Text(viewModel.email)
                    .fontWeight(.bold)
                
                Spacer()
                
                if !viewModel.friends.isEmpty {
                    NavigationLink(
                        destination: FriendsListView(friends: viewModel.friends),
                        label: {
                            Text("Friends")
                                .fontWeight(.bold)
                                .foregroundColor(.white)
                                .padding(.vertical, 15)
                                .padding(.horizontal, 35)
                                .background(Color.black)
                                .clipShape(Capsule())
                        }
                    )
                }
            }
            .padding(.bottom, 30)
            .navigationTitle("Login Screen")
        }
        .onAppear {
            viewModel.fetchFriends()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
