//
//  AppDelegate.swift
//  FacebookPhotos
//
//  Created by Rahul Rawat on 18/06/21.
//

// Swift
//
// AppDelegate.swift
import UIKit
import FBSDKCoreKit

class AppDelegate: UIResponder, UIApplicationDelegate {
    
    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        
        ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions: launchOptions
        )

        return true
    }
}
