//
//  DefaultsHelper.swift
//  FacebookPhotos
//
//  Created by Rahul Rawat on 22/06/21.
//

import Foundation
import Combine

@propertyWrapper class UserDefaultsBacked<Value> {
    @Published var value: Value
    
    private let key: String
    private let storage: UserDefaults
    
    private static func getValueInDefaults<Value>(key: String, storage: UserDefaults) -> Value? {
        storage.value(forKey: key) as? Value
    }
    
    init(wrappedValue defaultValue: Value,
         key: String,
         storage: UserDefaults = .standard) {
        self.key = key
        self.storage = storage
        self.value = UserDefaultsBacked.getValueInDefaults(key: key, storage: storage) ?? defaultValue
    }
    
    var wrappedValue: Value {
        get {
            let value: Value? = UserDefaultsBacked.getValueInDefaults(key: key, storage: storage)
            return value ?? self.value
        }
        set {
            if let optional = newValue as? AnyOptional, optional.isNil {
                storage.removeObject(forKey: key)
            } else {
                storage.setValue(newValue, forKey: key)
            }
            self.value = newValue
        }
    }
    
    var projectedValue: AnyPublisher<Value, Never> {
        return $value
          .receive(on: DispatchQueue.main)
          .eraseToAnyPublisher()
      }
}

extension UserDefaultsBacked where Value: ExpressibleByNilLiteral {
    convenience init(key: String, storage: UserDefaults = .standard) {
        self.init(wrappedValue: nil, key: key, storage: storage)
    }
}

private protocol AnyOptional {
    var isNil: Bool { get }
}

extension Optional: AnyOptional {
    var isNil: Bool { self == nil }
}

class DefaultsHelper {
    private let defaults = UserDefaults.standard
    
    static let shared = DefaultsHelper()
    
    private init() { }
    
    @UserDefaultsBacked(key: "LOGGED_KEY")
    var isLoggedIn = false
    
    @UserDefaultsBacked(key: "LOGGED_EMAIL")
    var email: String = ""
    
    @UserDefaultsBacked(key: "USER_ID_KEY")
    var userId: String = ""
    
    func clearAll() {
        self.isLoggedIn = false
        self.email = ""
        self.userId = ""
    }
}
