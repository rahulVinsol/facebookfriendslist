//
//  FriendsInfo.swift
//  FacebookPhotos
//
//  Created by Rahul Rawat on 22/06/21.
//

import Foundation

// MARK: - FriendsInfo
struct FriendsInfo: Codable {
    let data: [Friend]
    let paging: Paging
    let summary: Summary
}

// MARK: - Friend
struct Friend: Codable, Identifiable {
    let picture: Picture
    let name, id: String
}

// MARK: - Picture
struct Picture: Codable {
    let data: DataClass
}

// MARK: - DataClass
struct DataClass: Codable {
    var url: String
}

// MARK: - Paging
struct Paging: Codable {
    let cursors: Cursors
}

// MARK: - Cursors
struct Cursors: Codable {
    let before, after: String
}

// MARK: - Summary
struct Summary: Codable {
    let totalCount: Int

    enum CodingKeys: String, CodingKey {
        case totalCount = "total_count"
    }
}

