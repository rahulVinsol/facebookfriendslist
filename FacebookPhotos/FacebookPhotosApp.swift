//
//  FacebookPhotosApp.swift
//  FacebookPhotos
//
//  Created by Rahul Rawat on 17/06/21.
//

import SwiftUI
import FBSDKCoreKit

@main
struct FacebookPhotosApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .onOpenURL(perform: { url in
                    ApplicationDelegate.shared.application(UIApplication.shared, open: url, sourceApplication: nil, annotation: UIApplication.OpenURLOptionsKey.annotation)
                    
                })
        }
    }
}
