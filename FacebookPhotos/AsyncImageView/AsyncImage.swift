//
//  AsyncImage.swift
//  FacebookPhotos
//
//  Created by Rahul Rawat on 22/06/21.
//

import SwiftUI

struct AsyncImage<Placeholder: View>: View {
    @ObservedObject private var viewModel: AsyncImageViewModel = AsyncImageViewModel()
    private let placeholder: Placeholder
    private let url: String
    private let imageClosure: (UIImage) -> Image
    private let cacheKey: String?
    
    init(
        url: String,
        cacheKey: String? = nil,
        placeholder: Placeholder,
        @ViewBuilder imageClosure: @escaping (UIImage) -> Image
    ) {
        self.cacheKey = cacheKey
        self.url = url
        self.placeholder = placeholder
        self.imageClosure = imageClosure
    }
    
    var body: some View {
        content
            .onAppear {
                viewModel.loadImage(url: url, cacheKey: cacheKey)
            }
    }
    
    private var content: some View {
        Group {
            if let image = viewModel.image {
                imageClosure(image)
            } else {
                placeholder
            }
        }
    }
}
