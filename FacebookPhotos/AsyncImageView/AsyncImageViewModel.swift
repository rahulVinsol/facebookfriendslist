//
//  AsyncImageViewModel.swift
//  FacebookPhotos
//
//  Created by Rahul Rawat on 22/06/21.
//

import SwiftUI

class AsyncImageViewModel: ObservableObject {
    private var imageDownloader = ImageDownloadUtility.shared
    
    @Published var image: UIImage? = nil
    
    func loadImage(url: String, cacheKey: String?) {
        imageDownloader.download(url: url, cacheKey: cacheKey) { image in
            self.image = image
        }
    }
}
