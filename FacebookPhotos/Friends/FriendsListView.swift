//
//  FriendsListView.swift
//  FacebookPhotos
//
//  Created by Rahul Rawat on 22/06/21.
//

import SwiftUI

struct FriendsListView: View {
    @State var friends: [Friend]!
    @State private var selectedFriend: Friend? = nil
    
    var body: some View {
        List(friends) { friend in
            HStack {
                AsyncImage(url: friend.picture.data.url, cacheKey: friend.id, placeholder: Color.black) { image in
                    Image(uiImage: image)
                        .resizable()
                }
                .frame(width: 50, height: 50, alignment: .center)
                .clipShape(Circle())
                
                Text(friend.name)
                    .padding(.leading, 10)
                    .padding(.trailing, 10)
            }.onTapGesture {
                self.selectedFriend = friend
            }
        }
        .sheet(item: $selectedFriend) { friend in
            FriendInfoView(friend: friend)
        }
        .navigationTitle("Friends")
    }
}
